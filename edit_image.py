import requests
from PIL import Image, ImageFilter
import os

THIS_FILE = os.path.abspath(__file__)
BASE_DIR = os.path.dirname(THIS_FILE)
DOWNLOAD_DIR = os.path.join(BASE_DIR, "downloads")
os.makedirs(DOWNLOAD_DIR, exist_ok=True)
directory = DOWNLOAD_DIR

url = "https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-1100x628.jpg"
bandw = True

    # BLUR
    # CONTOUR
    # DETAIL
    # EDGE_ENHANCE
    # EDGE_ENHANCE_MORE
    # EMBOSS
    # FIND_EDGES
    # SHARPEN
    # SMOOTH
    # SMOOTH_MORE
#filters = ImageFilter.DETAIL
filters = None

#download image
def download(directory, url):
    filename = os.path.basename(url)
    download_img_path = os.path.join(directory, filename)
    r = requests.get(url, stream=True)
    r.raise_for_status()
    with open(download_img_path, 'wb') as f:
        f.write(r.content)
    return download_img_path

img_path= download(directory, url)

resolution = 1080, 720
#edit image
def edit_img(img_path, resolution, bandw, filters):
    size = resolution
    filename = os.path.basename(img_path)
    clean_name = os.path.splitext(filename)[0]
    print(clean_name)
    out1 = Image.open(img_path)
    if filters:
        out1= out1.filter(filters)
    if bandw:
        out1 = out1.convert('L')
    print(img_path)
    out1.thumbnail(size)
    out1.show()
    print(img_path)
    out1.save(f'edited/{clean_name}.png', 'png')

edit_img(img_path,resolution, bandw, filters)
